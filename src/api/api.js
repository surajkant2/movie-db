import axios from "axios";

const api = axios.create({
  baseURL: "https://api.themoviedb.org/3",
  params: {
    api_key: process.env.REACT_APP_API_KEY,
  },
});

export const imageUrl = (path, size = "original") => {
  return `https://image.tmdb.org/t/p/${size}${path}`;
};

export const getMovies = (page, query) => {
  if (query) {
    return api.get("/search/movie", {
      params: {
        query: query,
        page: page,
      },
    });
  } else {
    return api.get("/trending/movie/week", {
      params: {
        page: page,
      },
    });
  }
};

export const getMovieDetails = (movieId) => {
  return api.get(`/movie/${movieId}`);
};

export const getMovieTrailerTeaser = (movieId) => {
  return api.get(`/movie/${movieId}/videos`);
};

export const getRequestToken = () => {
  return api.get(`/authentication/token/new`);
};

export const login = (loginDetails) => {
  return api.post(`/authentication/token/validate_with_login`, loginDetails);
};

export const isLoggedIn = () => {
  const tokenDetails = JSON.parse(localStorage.getItem("movie_db"));
  if (
    tokenDetails?.token &&
    tokenDetails?.expires_at &&
    new Date(Date.now()) < new Date(tokenDetails?.expires_at)
  ) {
    return true;
  } else {
    localStorage.removeItem("movie_db");
    return false;
  }
};

export const setTokenToLocal = (token, expires_at) => {
  localStorage.setItem(
    "movie_db",
    JSON.stringify({
      token,
      expires_at,
    })
  );
};

export const logout = () => {
  localStorage.removeItem("movie_db");
};
