import { Link, useLocation, useNavigate } from "react-router-dom";
import instaPlayLogo from "../images/insta-play-logo.svg";
import searchIcon from "../images/search-icon.svg";
import { isLoggedIn, logout } from "../api/api";

const Navbar = ({formValue, handleFormValueChange}) => {
  const navigate = useNavigate()
  const location = useLocation()

  return (
    <div className="Navbar">
      <div className="container">
        <div className="nav-wrap">
          <Link className="logo" to="/">
            <img src={instaPlayLogo} alt="Insta Play" />
          </Link>

          {isLoggedIn() && (
            <div className="logout-mobile">
              <button
                onClick={() => {
                  logout();
                  navigate("/");
                }}
                className="logout-button"
              >
                Logout
              </button>
            </div>
          )}

          {isLoggedIn() && (
            <>
              <div className="search-and-logout flex">
                {location.pathname === "/movies" && (
                  <form
                    className="search-form flex"
                    onSubmit={(e) => e.preventDefault()}
                  >
                    <input
                      type="text"
                      placeholder="Search movies"
                      id="searchbar"
                      value={formValue}
                      onChange={handleFormValueChange}
                    />
                    <label htmlFor="searchbar" className="search-icon">
                      <img src={searchIcon} alt="Search..." />
                    </label>
                  </form>
                )}

                <button
                  onClick={() => {
                    logout();
                    navigate("/");
                  }}
                  className="logout-button"
                >
                  Logout
                </button>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Navbar;
