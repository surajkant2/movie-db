import { useState } from "react";
import {
  getRequestToken,
  login,
  setTokenToLocal,
} from "../api/api";
import ButtonLoader from "./ButtonLoader";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const Login = () => {
  const [formState, setFormState] = useState({
    username: {
      value: "",
      focusedOrNotEmpty: false,
    },
    password: {
      value: "",
      focusedOrNotEmpty: false,
    },
  });

  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [mainError, setMainError] = useState("");
  const [loading, setLoading] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      setLoading(true);
      try {
        const tokenResponse = await getRequestToken();
        const loginResponse = await login({
          username: formState.username.value,
          password: formState.password.value,
          request_token: tokenResponse.data.request_token,
        });
        if (loginResponse.data.success) {
          setTokenToLocal(
            loginResponse.data.request_token,
            tokenResponse.data.expires_at
          );
          toast.success("Login successfull", {
            position: "bottom-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          navigate("/movies");
        }
        setLoading(false);
      } catch (error) {
        setMainError(error?.response?.data?.status_message || "Login failed");
        setLoading(false);
      }
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState({
      ...formState,
      [name]: {
        ...formState[name],
        value: value,
      },
    });
  };

  const handleFocus = (e) => {
    const { name } = e.target;
    setFormState({
      ...formState,
      [name]: {
        ...formState[name],
        focusedOrNotEmpty: true,
      },
    });
  };

  const validate = () => {
    let valid = true;
    setUsernameError("");
    setPasswordError("");
    setMainError("");

    if (!formState.username.value.trim()) {
      setUsernameError("Please enter your username");
      valid = false;
    }

    if (!formState.password.value.trim()) {
      setPasswordError("Please enter your password");
      valid = false;
    }

    return valid;
  };

  const handleBlur = (e) => {
    const { name, value } = e.target;
    if (!value) {
      setFormState({
        ...formState,
        [name]: {
          ...formState[name],
          focusedOrNotEmpty: false,
        },
      });
    }
  };

  return (
    <div className="container">
      <div className="Login">
        <div className="login-box">
          <h1>Sign in</h1>
          <p className="instruction">Sign in to your Self Service Portal</p>
          <form onSubmit={handleSubmit}>
            <div className="input-and-error-wrap">
              <div className="input-wrap">
                <label
                  htmlFor="username"
                  className={`floating-placeholder ${
                    formState.username.focusedOrNotEmpty ? " floated" : ""
                  }`}
                >
                  Username
                </label>
                <input
                  type="text"
                  id="username"
                  name="username"
                  onChange={handleChange}
                  onFocus={handleFocus}
                  onBlur={handleBlur}
                  value={formState.username.value}
                />
              </div>
              {usernameError && <p className="error-text">{usernameError}</p>}
            </div>

            <div className="input-and-error-wrap">
              <div className="input-wrap">
                <label
                  htmlFor="password"
                  className={`floating-placeholder ${
                    formState.password.focusedOrNotEmpty ? " floated" : ""
                  }`}
                >
                  Password
                </label>
                <input
                  type="password"
                  id="password"
                  name="password"
                  onChange={handleChange}
                  onFocus={handleFocus}
                  onBlur={handleBlur}
                  value={formState.password.value}
                />
              </div>
              {passwordError && <p className="error-text">{passwordError}</p>}
            </div>
            {mainError && <p className="error-text">{mainError}</p>}
            <button disabled={loading} type="submit" className="login-button">
              <span>LOG IN</span>
              {loading && <ButtonLoader />}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
