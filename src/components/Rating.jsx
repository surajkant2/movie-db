import star from '../images/star-icon.svg';

const Rating = ({rating, total}) => {
    const int = parseInt(rating);
    const fraction = rating - int;
    const stars = [];

    for(let i = 0; i < int; i++){
        stars.push(<div key={i} style={{
            width: 15,
            height: 15,
            marginRight: 5,
            backgroundImage: `url(${star})`,
            backgroundPosition: 'left center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 15,
        }}></div>)
    }

    if(fraction){
        stars.push(<div key={'f'} style={{
            width: 15 * fraction,
            height: 15,
            marginRight: 5,
            backgroundImage: `url(${star})`,
            backgroundPosition: 'left center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 15,
        }}></div>)
    }

    return (<div className="flex">
        <div className='flex'>{stars}</div>
        <div
            style={{
                fontFamily: "'Poppins', sans-serif",
                fontSize: 14,
                marginLeft: rating ? 7 : 0,
            }}
            className="rating-text"
        >{rating} / {total}</div>
    </div>);
}
 
export default Rating;